﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmployeeWebApp.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EmployeeWebApp.Controllers
{
    public class HomeController : Controller
    {
        IMongoDatabase database;
        public IMongoDatabase GetMongoDatabase()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            return mongoClient.GetDatabase("SampleEmployeeDb");
        }
        
        public IActionResult Index()
        {
            return View();
        }

       

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        [HttpGet]
        public IActionResult AddEmp()
        {
            return View();
        }
        [HttpGet]
        public IActionResult GetbyDeptId()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Department()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Department(string department_id)
        {
            database = GetMongoDatabase();

            var result = database.GetCollection<Employee>("Employee").Find(s => s.department_id == department_id).ToList();
            
            return View(result);

        }
        [HttpGet]
        public IActionResult EditById()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Edit(Employee employee)
        {
            try
            {

                database = GetMongoDatabase();
                var result=database.GetCollection<Employee>("Employee").FindOneAndReplace(s => s._id == employee._id, employee);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult AddEmp(Employee employee)
        {
            try
            {

                database = GetMongoDatabase();
                database.GetCollection<Employee>("Employee").InsertOne(employee);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Viewall()
        {
            database = GetMongoDatabase();

            var result = database.GetCollection<Employee>("Employee").Find(s => true).ToList();
            var result2 = result.OrderBy(s => s._id).ToList();
            return View(result2);
        }
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        public IActionResult Delete()
        {
           
            return View();
        }
        [HttpPost]
        public IActionResult Delete(int _id)
        {
            database = GetMongoDatabase();
            var emp = database.GetCollection<Employee>("Employee").Find(s => s._id == _id).FirstOrDefault();
            if (emp == null)
            {
                //return NotFound($"Employee not found " + _id);
                return RedirectToAction("Delete");
            }
            try
            {

                database.GetCollection<Employee>("Employee").DeleteOne(s=>s._id==_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok($"Employee deleted with eid "+_id);
        }
    }
}
